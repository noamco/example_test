@extends('layouts.app')
@section('content')
<h1>Create a new Task</h1>
<form method = 'POST' action ="{{action('TaskController@store')}}">
@csrf
<div class = "form-group">
<label for = "title">Task name:</label>
<input type = "text" class= "form-control" name = "title">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>

<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


</form>
@endsection