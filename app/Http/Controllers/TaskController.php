<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;//verification



        /*        if (Auth::guest()) {
            abort(403,"You have no permission to enter");
        }*/

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            // The user is logged in...
        
        //$id= Auth::id();
        //$user = User::find($id);
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
        }
        return redirect()->intended('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
        return view ('tasks.create');
        }
    return redirect()->intended('/home');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
//add following if asked for verification

        $this->validate($request,[
            'title'=>'required',
            ]
        );
//
                
        $task = new Task();
        $id = Auth::id(); //the id of the current user
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status = 0;
        $task->save();
        return redirect('tasks');  
    }
    return redirect()->intended('/home');
     }
  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
        $task = Task::find($id);
    return view('tasks.edit', compact('task'));
        }
    return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {

//add following if asked for verification

       if (Auth::check()) {
        $this->validate($request,[
            'title'=>'required',
            ]
//
        );

       $task = Task::find($id);
       $task->update($request->except(['_token']));
  
      if($request->ajax()){
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to check tasks");
       }
           return Response::json(array('result' => 'success1','status' => $request->status ), 200);
       } else {          
           return redirect('tasks');           
       }
        }
    return redirect()->intended('/home');
   }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to delete tasks");
       }
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');
        }
    return redirect()->intended('/home');
    }

    public function mytasks()
    { 
        if (Auth::check()) {
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = $user->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
    }
    return redirect()->intended('/home');
    }

    public function done($id)
    {
        if (Auth::check()) {
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
        }
    return redirect()->intended('/home');
    }
}
