<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [    
                [
                'name' => 'aa',
                'email' => 'a@a.com',
                'role' => 'admin',
                'password'=> Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                ],
            ]
        );
    }
}
