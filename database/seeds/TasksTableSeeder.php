<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
            [    
                [
                'title' => 'task1',
                'user_id'=> 1,
                'status'=>0
                ],
                [
                'title' => 'task2',
                'user_id'=> 1,
                'status'=>0

                ],
                [
                'title' => 'task3',
                'user_id'=> 1,
                'status'=>0

                ],
            ]
            );
    }
}
